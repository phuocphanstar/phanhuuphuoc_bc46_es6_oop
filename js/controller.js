//Reset form
function formRS() {
  document.getElementById("formNV").reset();
}
function hideModal(){
  $('#myModal').modal('hide')
}
function resetTb() {
  document.getElementById("tbTKNV").innerHTML = "";
  document.getElementById("tbTen").innerHTML = "";
  document.getElementById("tbAddress").innerHTML = "";
  document.getElementById("tbEmail").innerHTML = "";
  document.getElementById("tbMath").innerHTML = "";
  document.getElementById("tbPhysics").innerHTML = "";
  document.getElementById("tbChemistry").innerHTML = "";
  document.getElementById("tbworkDays").innerHTML = "";
  document.getElementById("tbdaily").innerHTML = "";
  document.getElementById("tbinvoice").innerHTML = "";
  document.getElementById("tbrating").innerHTML = "";
}
function showModalFea() {
  resetTb();
  let chucVu = document.getElementById("chucvu").value;
  let math = document.getElementById("math");
  let phy = document.getElementById("phy");
  let chem = document.getElementById("chem");
  let wDay = document.getElementById("work");
  let dSal = document.getElementById("salary");
  let coName = document.getElementById("comp");
  let inV = document.getElementById("invoi");
  let rat = document.getElementById("rate");
  if (chucVu === "student") {
    math.style.display = "block";
    phy.style.display = "block";
    chem.style.display = "block";
    wDay.style.display = "none";
    dSal.style.display = "none";
    coName.style.display = "none";
    inV.style.display = "none";
    rat.style.display = "none";
  } else if (chucVu === "staff") {
    math.style.display = "none";
    phy.style.display = "none";
    chem.style.display = "none";
    wDay.style.display = "block";
    dSal.style.display = "block";
    coName.style.display = "none";
    inV.style.display = "none";
    rat.style.display = "none";
  } else if (chucVu === "customer") {
    math.style.display = "none";
    phy.style.display = "none";
    chem.style.display = "none";
    wDay.style.display = "none";
    dSal.style.display = "none";
    coName.style.display = "block";
    inV.style.display = "block";
    rat.style.display = "block";
  } else {
    math.style.display = "none";
    phy.style.display = "none";
    chem.style.display = "none";
    wDay.style.display = "none";
    dSal.style.display = "none";
    coName.style.display = "none";
    inV.style.display = "none";
    rat.style.display = "none";
  }
}
//render DSNV
function renderDSNV(dsns) {
  contentHTML = "";
  for (var i = 0; i < dsns.length; i++) {
    var ns = dsns[i];
    if (ns.type === "student") {
      content = `<tr>
        <td>${ns.id}</td>
        <td>${ns.name}</td>
        <td>${ns.address}</td>
        <td>${ns.email}</td>
        <td>${ns.calculateAverage()}</td>
        <td>_</td>
        <td>_</td>
        <td>_</td>
        <td><button data-toggle="modal"
        data-target="#myModal" onclick="suaNS('${
          ns.id
        }')" class="btn btn warning" my-2">Sửa</button> 
        <button onclick="xoaNS('${ns.id}')" class="btn btn-danger">Xoá</button>
        </td>
        </tr>`;
    } else if (ns.type === "staff") {
      content = `<tr>
        <td>${ns.id}</td>
        <td>${ns.name}</td>
        <td>${ns.address}</td>
        <td>${ns.email}</td>
        <td>_</td>
        <td>${ns.calculateSalary()}</td>
        <td>_</td>
        <td>_</td>
        <td><button data-toggle="modal"
        data-target="#myModal" onclick="suaNS('${
          ns.id
        }')" class="btn btn warning" my-2">Sửa</button> 
        <button onclick="xoaNS('${ns.id}')" class="btn btn-danger">Xoá</button>
        </td>
        </tr>`;
    } else {
      content = `<tr>
        <td>${ns.id}</td>
        <td>${ns.name}</td>
        <td>${ns.address}</td>
        <td>${ns.email}</td>
        <td>_</td>
        <td>_</td>
        <td>${ns.companyName}</td>
        <td>${ns.invoiceValue}</td>
        <td><button data-toggle="modal"
        data-target="#myModal" onclick="suaNS('${ns.id}')" class="btn btn warning" my-2">Sửa</button> 
        <button onclick="xoaNS('${ns.id}')" class="btn btn-danger">Xoá</button>
        </td>
        </tr>`;
    }
    contentHTML += content;
  }
  document.getElementById("tableDanhSach").innerHTML = contentHTML;
}

function layThongTinTuForm() {
  var type = document.getElementById("chucvu").value;
  var id = document.getElementById("tknv").value;
  var name = document.getElementById("name").value;
  var email = document.getElementById("email").value;
  var address = document.getElementById("address").value;
  var math = Number(document.getElementById("mathGrade").value);
  var physics = Number(document.getElementById("physicsGrade").value);
  var chem = Number(document.getElementById("chemisGrade").value);
  var workDays = Number(document.getElementById("worksDays").value);
  var dailySal = Number(document.getElementById("dailySalary").value);
  var compName = document.getElementById("compName").value;
  var inVoice = Number(document.getElementById("inVoice").value);
  var rating = document.getElementById("rating").value;
  if (type === "student") {
    return new Student(name, address, id, email, type, math, physics, chem);
  } else if (type === "staff") {
    return new Employee(name, address, id, email, type, workDays, dailySal);
  } else if (type === "customer") {
    return new Customer(
      name,
      address,
      id,
      email,
      type,
      compName,
      inVoice,
      rating
    );
  }
}
// //Lưu data xuống local
// function luuData(dsnv) {
//   var dataJSON = JSON.stringify(dsnv);
//   localStorage.setItem("DSNV", dataJSON);
// }
//Show thông tin lên form
function showThongTinLenForm(nv) {
  // phải là .value chứ k phải .innerHTML
  document.getElementById("chucvu").value = nv.type;
  document.getElementById("tknv").value = nv.id;
  document.getElementById("name").value = nv.name;
  document.getElementById("email").value = nv.email;
  document.getElementById("address").value = nv.address;
  document.getElementById("mathGrade").value = nv.math;
  document.getElementById("physicsGrade").value = nv.physics;
  document.getElementById("chemisGrade").value = nv.chemistry;
  document.getElementById("worksDays").value = nv.workDays;
  document.getElementById("dailySalary").value = nv.dailySalary;
  document.getElementById("compName").value = nv.companyName;
  document.getElementById("inVoice").value = nv.invoiceValue;
  document.getElementById("rating").value = nv.rating;
}
var testTask ={
 kiemTraFormWithOutID:function(nv){
  //Ưu tiên validate để trống trước
  //Validate name
  var isNameValid = kiemTraTrong(nv.name, "tbTen") && kiemTraTen(nv.name);

  //Validate email
  var isEmailValid =
    kiemTraTrong(nv.email, "tbEmail") &&
    kiemTraEmail(nv.email) &&
    kiemTraKhoangCach(nv.email, "tbEmail");

  //Validate địa chỉ
  var isAddressValid = kiemTraTrong(nv.address, "tbAddress");

  //Validate điểm toan
  var isMathValid =
    kiemTraTrong(nv.math, "tbMath") &&
    kiemTraLaSo(nv.math, "tbMath") &&
    kiemTraMucGiaTri(10, 0, "tbMath", nv.math, "Điểm phải nằm từ 0-10");

  //Validate điểm lý
  var isPhysicsValid =
    kiemTraTrong(nv.physics, "tbPhysics") &&
    kiemTraLaSo(nv.physics, "tbPhysics") &&
    kiemTraMucGiaTri(10, 0, "tbPhysics", nv.physics, "Điểm phải nằm từ 0-10");

  //Validate điểm hoá
  var isChemValid =
    kiemTraTrong(nv.chemistry, "tbChemistry") &&
    kiemTraLaSo(nv.chemistry, "tbChemistry") &&
    kiemTraMucGiaTri(
      10,
      0,
      "tbChemistry",
      nv.chemistry,
      "Điểm phải nằm từ 0-10"
    );

  //Validate số ngày làm
  var isDayValid =
    kiemTraTrong(nv.workDays, "tbworkDays") &&
    kiemTraLaSo(nv.workDays, "tbworkDays") &&
    kiemTraMucGiaTri(
      30,
      0,
      "tbworkDays",
      nv.workDays,
      "Giới hạn giờ làm từ 80 - 200"
    );

  //Validate lương ngày
  var isSalaryValid =
    kiemTraTrong(nv.dailySalary, "tbdaily") &&
    kiemTraLaSo(nv.dailySalary, "tbdaily") &&
    kiemTraMucGiaTri(
      1000000,
      100000,
      "tbdaily",
      nv.dailySal,
      "Tiền lương 1 ngày từ 100.000 - 1.000.000"
    );

  // Validate tên công ty
  var isCompValid = kiemTraTrong(nv.companyName, "tbcompName");

  // Validate invoice
  var isInvoiceValid =
    kiemTraTrong(nv.invoiceValue, "tbinvoice") &&
    kiemTraLaSo(nv.invoiceValue, "tbinvoice");

  // Validate rating
  var isRatingValid = kiemTraTrong(nv.rating, "tbrating");

  if (nv.type === "student") {
    return (
      isNameValid &
      isEmailValid &
      isAddressValid &
      isMathValid &
      isPhysicsValid &
      isChemValid
    );
  } else if (nv.type === "staff") {
    return (
      isNameValid &
      isEmailValid &
      isAddressValid &
      isDayValid &
      isSalaryValid
    );
  } else if (nv.type === "customer") {
    return (
      isNameValid &
      isEmailValid &
      isAddressValid &
      isCompValid &
      isInvoiceValid &
      isRatingValid
    );
  }
  else{
    return document.getElementById("tbChucVu").innerHTML="Vui lòng chọn phân loại"
  }
},
 kiemTraFormWithID:function(nv, dsnv) {
  //Ưu tiên validate để trống trước
  //Validate id
  var isIdValid =
    kiemTraTrong(nv.id, "tbTKNV") &&
    kiemTraTrung(dsnv, nv.id, "tbTKNV", "ID đã tồn tại") &&
    kiemTraKhoangCach(nv.id, "tbTKNV");

  //Validate name
  var isNameValid = kiemTraTrong(nv.name, "tbTen") && kiemTraTen(nv.name);

  //Validate email
  var isEmailValid =
    kiemTraTrong(nv.email, "tbEmail") &&
    kiemTraEmail(nv.email) &&
    kiemTraKhoangCach(nv.email, "tbEmail");

  //Validate địa chỉ
  var isAddressValid = kiemTraTrong(nv.address, "tbAddress");

  //Validate điểm toan
  var isMathValid =
    kiemTraTrong(nv.math, "tbMath") &&
    kiemTraLaSo(nv.math, "tbMath") &&
    kiemTraMucGiaTri(10, 0, "tbMath", nv.math, "Điểm phải nằm từ 0-10");

  //Validate điểm lý
  var isPhysicsValid =
    kiemTraTrong(nv.physics, "tbPhysics") &&
    kiemTraLaSo(nv.physics, "tbPhysics") &&
    kiemTraMucGiaTri(10, 0, "tbPhysics", nv.physics, "Điểm phải nằm từ 0-10");

  //Validate điểm hoá
  var isChemValid =
    kiemTraTrong(nv.chemistry, "tbChemistry") &&
    kiemTraLaSo(nv.chemistry, "tbChemistry") &&
    kiemTraMucGiaTri(
      10,
      0,
      "tbChemistry",
      nv.chemistry,
      "Điểm phải nằm từ 0-10"
    );

  //Validate số ngày làm
  var isDayValid =
    kiemTraTrong(nv.workDays, "tbworkDays") &&
    kiemTraLaSo(nv.workDays, "tbworkDays") &&
    kiemTraMucGiaTri(
      30,
      0,
      "tbworkDays",
      nv.workDays,
      "Giới hạn giờ làm từ 80 - 200"
    );

  //Validate lương ngày
  var isSalaryValid =
    kiemTraTrong(nv.dailySalary, "tbdaily") &&
    kiemTraLaSo(nv.dailySalary, "tbdaily") &&
    kiemTraMucGiaTri(
      1000000,
      100000,
      "tbdaily",
      nv.dailySalary,
      "Tiền lương 1 ngày từ 100.000 - 1.000.000"
    );

  // Validate tên công ty
  var isCompValid = kiemTraTrong(nv.companyName, "tbcompName");

  // Validate invoice
  var isInvoiceValid =
    kiemTraTrong(nv.invoiceValue, "tbinvoice") &&
    kiemTraLaSo(nv.invoiceValue, "tbinvoice");

  // Validate rating
  var isRatingValid = kiemTraTrong(nv.rating, "tbrating");

  if (nv.type === "student") {
    return (
      isIdValid &
      isNameValid &
      isEmailValid &
      isAddressValid &
      isMathValid &
      isPhysicsValid &
      isChemValid
    );
  } else if (nv.type === "staff") {
    return (
      isIdValid &
      isNameValid &
      isEmailValid &
      isAddressValid &
      isDayValid &
      isSalaryValid
    );
  } else if (nv.type === "customer") {
    return (
      isIdValid &
      isNameValid &
      isEmailValid &
      isAddressValid &
      isCompValid &
      isInvoiceValid &
      isRatingValid
    );
  }
  else{
    return document.getElementById("tbChucVu").innerHTML="Vui lòng chọn phân loại"
  }
}
}
