class Person {
  constructor(name, address, id, email,type) {
    this.name = name;
    this.address = address;
    this.id = id;
    this.email = email;
    this.type=type;
  }
}

class Student extends Person {
  constructor(name, address, id, email, type, math, physics, chemistry) {
    super(name, address, id, email, type);
    this.math = math;
    this.physics = physics;
    this.chemistry = chemistry;
  }
  calculateAverage() {
    return (this.math + this.physics + this.chemistry) / 3;
  }
}

class Employee extends Person {
  constructor(name, address, id, email, type, workDays, dailySalary) {
    super(name, address, id, email, type);
    this.workDays = workDays;
    this.dailySalary = dailySalary;
  }

  calculateSalary() {
    return this.workDays * this.dailySalary;
  }
}

class Customer extends Person {
  constructor(name, address, id, email, type, companyName, invoiceValue, rating) {
    super(name, address, id, email, type);
    this.companyName = companyName;
    this.invoiceValue = invoiceValue;
    this.rating = rating;
  }
}

class ListPerson {
  constructor() {
    this.persons = [];
  }

  addPerson(person) {
    this.persons.push(person);
  }

  removePerson(id) {
    const index = this.persons.findIndex(person => person.id === id);
    if (index !== -1) {
      this.persons.splice(index, 1);
      return true;
    }
    return false;
  }

  updatePerson(id, newPerson) {
    const index = this.persons.findIndex(person => person.id === id);
    if (index !== -1) {
      this.persons[index] = newPerson;
      return true;
    }
    return false;
  }

  sortByName() {
    return this.persons.sort((a, b) => a.name.localeCompare(b.name));
  }

  filterByType(personType) {
    return this.persons.filter(person => person.type === personType);
  }
}