//kiểm tra không để trống
function showMessage(id, message) {
  document.getElementById(id).innerHTML = message;
  document.getElementById(id).style.display = "block";
}
function kiemTraTrong(value, id) {
  if (value == "") {
    showMessage(id, "Vui lòng nhập dữ liệu!");
    return false;
  } else {
    showMessage(id, "");
    return true;
  }
}
function kiemTraTrung(value, input, id, message) {
  if (value.length == 0) {
    return true;
  } else {
    for (let i = 0; i < value.length; i++) {
      if (value[i].id === input) {
        showMessage(id, message);
        return false;
      } else {
        showMessage(id, "");
        return true;
      }
    }
  }
}
function kiemTraDoDai(max, min, id, value, message) {
  var length = value.length;
  if (length >= min && length <= max) {
    showMessage(id, "");
    return true;
  } else {
    showMessage(id, message);
    return false;
  }
}
function kiemTraMucGiaTri(max, min, id, value, message) {
  if (value >= min && value <= max) {
    showMessage(id, "");
    return true;
  } else {
    showMessage(id, message);
    return false;
  }
}
function kiemTraTen(value) {
  // const regex = /^[a-zA-ZÀ-ỹ]+$/;
  const regex = /^[\p{L} ]+$/u;
  regex.test(value);
  if (regex.test(value)) {
    showMessage("tbTen", "");
    return true;
  } else {
    showMessage("tbTen", "Tên không chính xác");
    return false;
  }
}
function kiemTraEmail(value) {
  const re =
    /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
  if (re.test(value)) {
    showMessage("tbEmail", "");
    return true;
  } else {
    showMessage("tbEmail", "Email sai cú pháp");
    return false;
  }
}
function kiemTraMatKhau(value) {
  const reg = /^(?=.*\d)(?=.*[A-Z])(?=.*\W).+$/;
  if (reg.test(value)) {
    showMessage("tbMatKhau", "");
    return true;
  } else {
    showMessage(
      "tbMatKhau",
      "Mật khẩu chứa ít nhất 1 ký tự số, 1 ký tự in hoa, 1 ký tự đặc biệt"
    );
    return false;
  }
}
function kiemTraNgay(value) {
  const rege = /^(0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/\d{4}$/;
  if (rege.test(value)) {
    showMessage("tbNgay", "");
    return true;
  } else {
    showMessage(
      "tbNgay",
      "Ngày sai định dạng, bạn có thể sử dụng Date picker trong giao diện"
    );
    return false;
  }
}
function kiemTraLaSo(value, id) {
  if (isNaN(value)) {
    //Nếu value là số thì trả ra true, k là số thì trả ra false
    showMessage(id, "Vui lòng nhập số");
    return false;
  } else {
    showMessage(id, "");
    return true;
  }
}
function kiemTraKhoangCach(value, id) {
  const regex = /\s/;
  if (regex.test(value)) {
    showMessage(id, "Không được có khoảng cách");
    return false;
  } else {
    showMessage(id, "");
    return true;
  }
}
