//Tạo mảng rỗng dùng khi load trang và lưu dữ liệu khi thêm sv
// var dsnv = [];
// //Xử lí khi load trang
// var dataJSON=localStorage.getItem("DSNV");
// if (dataJSON!=null){
//     dsnv=JSON.parse(dataJSON).map(function(item){
//         return new NhanVien(item.tk,item.hoTen,item.email,item.matKhau,item.ngayLam,item.luongCB,item.chucVu,item.gioLam);
//     });
//     renderDSNV(dsnv);
// }
const listPerson = new ListPerson();
//Thêm nhân viên
function themNhanVien() {
  var ns = layThongTinTuForm();
  var isValid = testTask.kiemTraFormWithID(ns,listPerson.persons);
  if(isValid){
    listPerson.addPerson(ns);
  //render dsnv lên table
  renderDSNV(listPerson.persons);
//   var dataJSON=JSON.stringify(dsnv);
//   localStorage.setItem("DSNV",dataJSON);
  formRS();
  hideModal();
  //Nhớ lưu xuống local storage là phải là dataJSON chứ k phải dsnv!!!!!
}
}

//Xoá nhân viên
function xoaNS(id){
    // var index=dsnv.findIndex(function(item){
    //    return item.tk==id; 
    // }); 
    // dsnv.splice(index,1); (đây là cách làm khi không có class quản lí)

    listPerson.removePerson(id);
    renderDSNV(listPerson.persons);
    // luuData(dsnv);
}

//Sửa nhân viên
function suaNS(id){
    var index=listPerson.persons.findIndex(function(item){
        return item.id===id;
    });
    showThongTinLenForm(listPerson.persons[index]);
    showModalFea();
    
}
 
//Cập nhật sinh viên
function capNhatNhanVien(){
    var ns=layThongTinTuForm();
    // var isValid = kiemTraForm(nv);
    // if (isValid){
    // var index=dsnv.findIndex(function(item){
    //     return item.tk==nv.tk;
    // });
    // dsnv[index]=nv; (khi không dùng class listPerson)
    if (testTask.kiemTraFormWithOutID(ns)){
    listPerson.updatePerson(ns.id,ns)
    renderDSNV(listPerson.persons);
    // luuData(dsnv);
    formRS();
}
}
// }
function timKiemNhanVien(){
    // --> cách trước khi có class listPerson quản lí
    // var value=document.getElementById("searchName").value;
    // var dsxl=[];
    // for (var i=0;i<dsnv.length;i++){
    //     var nv=dsnv[i];
    //     if (nv.XepLoai()==value){
    //         dsxl.push(dsnv[i]);
    //     }
    //     renderDSNV(dsxl);
    // }
    var value = document.getElementById("searchName").value;
    var dsFilter = listPerson.filterByType(value);
    renderDSNV(dsFilter);   
}
document.getElementById('btnTimNV').addEventListener('click',timKiemNhanVien);

function sapXepTheoTen(){
    let dssx = listPerson.sortByName();
    console.log(dssx);
    renderDSNV(dssx);
}
